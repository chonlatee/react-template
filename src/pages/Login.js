import React from 'react';
// import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Form, Input, Button, Checkbox } from 'antd';

import { login } from '../service/auth.service';

const Login = () => {
  const dispatch = useDispatch();

  const onFinish = async (values) => {
    try {
      const res = await login({
        username: 'test',
        password: 'test'
      });
      dispatch({
        type: 'LOGIN_SUCCESS',
        data: res
      });
      console.log('Success:', values, res);
    } catch (err) {
      console.log('err', err);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Form
      name="basic"
      labelCol={{
        span: 8
      }}
      wrapperCol={{
        span: 16
      }}
      initialValues={{
        remember: true
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <p>
        <br />
      </p>
      <p>
        <br />
      </p>
      <p>
        <br />
      </p>
      <Form.Item
        label="Username"
        name="username"
        rules={[
          {
            required: true,
            message: 'Please input your username!'
          }
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!'
          }
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16
        }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

Login.propTypes = {};

Login.defaultProps = {};

export default Login;
