import React, { useEffect } from 'react';
// import PropTypes from 'prop-types';
// import { Link } from 'react-router-dom';

import { getUsers } from '../service/user.service';
import { logout } from '../service/auth.service';

const Home = () => {
  const [state, setState] = React.useState([]);

  const initPage = async () => {
    try {
      const res = await getUsers();
      setState(res);
    } catch (err) {
      console.log('err', err);
    }
  };

  useEffect(() => {
    initPage();
  }, []); // eslint-disable-line

  const handlelogout = () => {
    logout();
    window.location.reload();
  };

  console.log('state >>>', state);
  return (
    <div className="container">
      <h1>Home Page</h1>
      <button
        onClick={() => {
          initPage();
        }}
      >
        reload
      </button>
      <br />
      <br />
      <br />
      <br />
      <p>
        <button
          onClick={() => {
            handlelogout();
          }}
        >
          logout
        </button>
      </p>
    </div>
  );
};

Home.propTypes = {};

export default Home;
