import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './App';

import * as serviceWorker from './serviceWorker';

import configureStore from './configureStore';
import setupInterceptors from './service/setupInterceptors';

import './styles/index.scss';
import 'antd/dist/antd.css';

const initialState = {};
const store = configureStore(initialState);

ReactDOM.render(
  <Provider store={store} key="provider">
    <App />
  </Provider>,
  document.getElementById('root')
);
setupInterceptors(store);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
