import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { useSelector } from 'react-redux';
import get from 'lodash/get';

import Routers from './Routers';
import Login from './pages/Login';

const App = () => {
  const isLogin = useSelector(state => get(state, 'auth.isLoggedIn', false));

  return (
    <Router>
      <>
        {!isLogin && <Login />}
        {isLogin && (
          <div className="wrap-page">
            <Routers />
          </div>
        )}
      </>
    </Router>
  );
};

export default App;
