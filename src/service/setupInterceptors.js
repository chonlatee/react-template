import axiosInstance from './api';
import TokenService from './token.service';
import { refreshToken } from '../actions/auth';

const setup = (store) => {
  console.log('store >>', store);
  axiosInstance.interceptors.request.use(
    (config) => {
      const token = TokenService.getLocaljwtToken();
      console.log('token >>>', token);
      if (token) {
        // eslint-disable-next-line
        config.headers['Authorization'] = 'Bearer ' + token; // for Spring Boot back-end
        // config.headers['x-access-token'] = token; // for Node.js Express back-end
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  const { dispatch } = store;
  axiosInstance.interceptors.response.use(
    (res) => {
      return res;
    },
    async (err) => {
      const originalConfig = err.config;

      if (originalConfig.url !== '/users/authenticate' && err.response) {
        // Access Token was expired
        // eslint-disable-next-line no-underscore-dangle
        if (err.response.status === 401 && !originalConfig._retry) {
          // eslint-disable-next-line no-underscore-dangle
          originalConfig._retry = true;

          try {
            const rs = await axiosInstance.post('/users/refresh-token', {
              // refreshToken: TokenService.getLocalRefreshToken()
            }, { withCredentials: true });

            const { jwtToken } = rs.data;

            dispatch(refreshToken(jwtToken));
            TokenService.updateLocaljwtToken(jwtToken);

            return axiosInstance(originalConfig);
          } catch (_error) {
            return Promise.reject(_error);
          }
        }
      }

      return Promise.reject(err);
    }
  );
};

export default setup;
