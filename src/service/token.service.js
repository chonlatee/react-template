const getLocalRefreshToken = () => {
  const user = JSON.parse(localStorage.getItem('user'));
  return user?.refreshToken;
};

const getLocaljwtToken = () => {
  const user = JSON.parse(localStorage.getItem('user'));
  return user?.jwtToken;
};

const updateLocaljwtToken = (token) => {
  const user = JSON.parse(localStorage.getItem('user'));
  user.jwtToken = token;
  localStorage.setItem('user', JSON.stringify(user));
};

const getUser = () => {
  return JSON.parse(localStorage.getItem('user'));
};

const setUser = (user) => {
  console.log('setUser', JSON.stringify(user));
  localStorage.setItem('user', JSON.stringify(user));
};

const removeUser = () => {
  localStorage.removeItem('user');
};

const TokenService = {
  getLocalRefreshToken,
  getLocaljwtToken,
  updateLocaljwtToken,
  getUser,
  setUser,
  removeUser
};

export default TokenService;
