import api from './api';
import TokenService from './token.service';

export const register = (username, email, password) => {
  return api.post('/auth/signup', {
    username,
    email,
    password
  });
};

export const login = (body) => {
  return api
    .post('/users/authenticate', body)
    .then((response) => {
      if (response.data.jwtToken) {
        TokenService.setUser(response.data);
      }

      return response.data;
    });
};

export const logout = () => {
  TokenService.removeUser();
};

export const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem('user'));
};
