import { REFRESH_TOKEN } from './actionType';

// import AuthService from '../service/auth.service';

export const refreshToken = jwtToken => (dispatch) => {
  dispatch({
    type: REFRESH_TOKEN,
    payload: jwtToken
  });
};
