import { REFRESH_TOKEN } from '../actions/actionType';

const user = JSON.parse(localStorage.getItem('user'));

const initialState = user
  ? { isLoggedIn: true, user }
  : { isLoggedIn: false, user: null };

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case REFRESH_TOKEN:
      return {
        ...state,
        user: { ...user, jwtToken: payload }
      };
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        isLoggedIn: true,
        user: action.data
      };
    default:
      return state;
  }
}
