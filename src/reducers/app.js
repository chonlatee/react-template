import * as ActionType from '../actions/actionType';

const innitialState = {
  isLoading: false,
  isLogin: false,
  userLogin: {}
};

export default (state = innitialState, action) => {
  switch (action.type) {
    case ActionType.INITAPP: {
      return {
        ...state
      };
    }
    default: {
      return state;
    }
  }
};
